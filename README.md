# Welcome to basic Android Test!

The main purpose of this test is measuring the technical skills and knowledge of Android developers.

## Project Description

As a mobile engineer you've been tasked with the development of an app for Game lovers. This first version (MVP) of the app will be very simple and limited to only show the list of games by game plataform.

## Functional Requirements

The first release of the app will be very limited in scope, but will serve as the foundation for future releases. You're going to develop now basically 2 screens:

**Game List** - Where the user will get the plataforms list from the service and change the list according to the selected category.

**Game Detail** - Where the user will see some details of the game selected.

![Game List](images/games.png?raw=true "Games List")

![Game Detail](images/game_detail.png?raw=true "Game Detail")

## Technical Requirements

You should see this project as an opportunity to create an app following modern development best practices, but also feel free to use your own app architecture preferences (coding standards, code organization, third-party libraries, etc).

### API

The API to consumes your app must be IGDB.
https://api.igdb.com/
You can use a Wrapper or normal usage of API. **The normal usage of API is better to measure your knowledge.**

## Deliverables
The project source code and dependencies should be made available in Bitbucket, Github or Gitlab as you wish. Here are the steps you should
follow:

1. Create a public repository.

2. Create a "development" branch and commit the code to it. Do not push the code to the master branch. 

3. Create a "screenshots" sub-folder and include one screenshot by screen created.

4. Include a README file that describes:
	* Special build instructions, if any;
	* List of third-party libraries used and short description of why/how they were used;
	
5. Once the work is complete, create a pull request and send us the link.

## Notes

Remember that you're writing code that will be reviewed, tested. 
Things to keep in mind:

* First of all, it should compile and run without errors.

* Make the tests described on https://developer.android.com/training/testing/fundamentals.

* Despite the project simplicity, don't ignore development and architecture best practices. It's expected that code architecture will be structured to support project growth.

We wish you good luck!